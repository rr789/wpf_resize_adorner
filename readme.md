Resize any WPF Control
===================

![video description](https://gitlab.com/rr789/wpf-resize-adorner/raw/7b5eb699c50e66121fe9f63617901a188f9ad930/demo_video.gif)
----------
To use
-------------
Project has a working demo.  Key files are: 

 - CResizeAdorner.cs
 - CResizeAdornerTriangle.xaml
 - CChildHelper.cs

Enable resizing on control with one line. 
```
//Set max resize to 200px max height, 300px max width.
new CResizeAdorner(button1, 200, 300); 
```

--------
Side note:
--------
Element position needs to stay fixed.  Future update may support moving elements on screen.   

--------
Credit:
--------
Heavily inspired by Denis Vuyka's [example](https://denisvuyka.wordpress.com/2007/10/15/wpf-simple-adorner-usage-with-drag-and-resize-operations).  Many thanks.  

